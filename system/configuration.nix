# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  nixpkgs.overlays = [
    (self: super: {
      vanta-agent = super.callPackage /home/flyingleafe/dotfiles/system/overlays/vanta-agent-pkg.nix {};
    })
  ];
  
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      # Battery suspend service
      ./suspend.nix
      # Vanta agent service
      /home/flyingleafe/dotfiles/system/overlays/vanta-service.nix
    ];

  # Fix NVMe suspend problem?
  boot.kernelParams = ["iommu=soft"];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.grub.useOSProber = true;

  boot.initrd.luks.devices = {
    cryptroot = {
  device = "/dev/disk/by-uuid/2caa5741-7c73-454c-adc1-11c16a3ff549";
  preLVM = true;
    };
  };

  # networking.hostName = "nixos"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.


  hardware.pulseaudio.enable = true;
  hardware.pulseaudio.package = pkgs.pulseaudioFull;

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking = {
    useDHCP = false;
    interfaces.enp4s0.useDHCP = true;
    interfaces.wlp5s0.useDHCP = true;

    firewall.enable = false;
    hostName = "flf-nixos";
    # wireless.enable = true;
    networkmanager.enable = true;
    
    # Hosts
    hosts = {
      "127.0.0.1" = ["auth.dev.local" "matrix.dev.local" "dev.local"];
    };

    # Wireguard VPN
    wg-quick.interfaces = {
      wg0 = {
        #address = [ "10.8.0.2/24" "10.100.0.3/24" ];
        autostart = false;
        address = [ "10.100.0.3/24" ];
        dns = [ "10.100.0.1" ];
        privateKeyFile = "/home/flyingleafe/private/private-data/keys/wireguard.sk";

        peers = [
          #{
          #  publicKey = "dU5CcHaqj8L8KCgUSF3iJbc+DIx19aGNUUl+Rlar13g=";
          #  endpoint = "137.184.11.123:51820";
          #  allowedIPs = [ "10.8.0.0/24" ];
          #  persistentKeepalive = 25;
          #}
          {
            # Dedibox VPN
            publicKey = "5XGmNoJG5qbZ8FOkm52WAgwnX7jE+/LEyw3fNeZLqFo=";
            endpoint = "51.15.181.195:51820";
            allowedIPs = [ "0.0.0.0/0" ];
            persistentKeepalive = 25;
          }
        ];
      };
      
#      wg1 = {
#        address = [ "10.8.0.2/24" ];
#        privateKeyFile = "/home/flyingleafe/private/private-data/keys/wireguard.sk";
#
#        peers = [
#          {
#            publicKey = "dU5CcHaqj8L8KCgUSF3iJbc+DIx19aGNUUl+Rlar13g=";
#            endpoint = "137.184.11.123:51820";
#            allowedIPs = [ "10.8.0.0/24" ];
#            persistentKeepalive = 25;
#          }
#        ];
#      };
    };
#
#    extraHosts =
#      ''
#        10.8.0.1 cometa-backend
#      '';
  };

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  # i18n = {
  #   consoleFont = "Lat2-Terminus16";
  #   consoleKeyMap = "us";
  #   defaultLocale = "en_US.UTF-8";
  # };

  time.timeZone = "Europe/London";

  fonts = {
    fontDir.enable = true;
    enableGhostscriptFonts = true;
    packages = with pkgs; [
      inconsolata  # monospaced
      source-code-pro
    ];
  };

  nix = {
    package = pkgs.nixVersions.nix_2_23;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
    settings = {
      sandbox = false;
      trusted-users = ["root" "flyingleafe"];
  
      substituters = [
        "https://cache.nixos.org"
        "https://disciplina.cachix.org"
        #"https://hydra.iohk.io"
        "https://iohk.cachix.org"
      ];

      trusted-substituters = [
        "http://hydra.cryp.to"
      ];
      trusted-public-keys = [
        #"hydra.nixos.org-1:CNHJZBh9K4tP3EKF6FkkgeVYsS3ohTl+oS0Qa8bezVs="
        "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY="
        "hydra.serokell.io-1:he7AKwJKKiOiy8Sau9sPcso9T/PmlVNxcnNpRgcFsps="
        "disciplina.cachix.org-1:zDeIFV5cu22v04EUuRITz/rYxpBCGKY82x0mIyEYjxE="
        #"hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ="
        "iohk.cachix.org-1:DpRUyj7h7V830dp/i6Nti+NEO2/nhblbov/8MW7Rqoo="
      ];
    };
  };

  nixpkgs.config = {
    allowUnfree = true;
    allowBroken = true;

    # chromium = {
        # enableWideVine = true;
    # };

    # firefox = {
      # enableAdobeFlash = true;
    # };

    permittedInsecurePackages = [
      "python2.7-urllib3-1.26.2"
      "python2.7-PyJWT-1.7.1" 
      "rxvt-2.7.10"
      "qtwebkit-5.212.0-alpha4"     
    ];
  };

  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  environment.systemPackages = with pkgs; [
    vim
    emacs
    firefox
    chromium
    brave
    #torbrowser
    wget
    curl
    rxvt
    rxvt_unicode
    keepassxc
    nox
    tdesktop
    ffmpeg-full
    atool
    vlc
    libnotify
    inotify-tools
    rsync
    calc
    gnugrep
    scrot
    deluge
    file
    sshfs-fuse
    commonsCompress
    unzip
    htop
    acpi
    acpid
    zathura
    mediainfo
    wireshark
    lsof
    keychain
    gtypist
    thunderbird
    stack
    gnupg
    gnupg1compat
    pinentry
    dropbox-cli
    unrar
    weechat
    openssl
    imagemagick
    mplayer
    gqview
    libreoffice
    libsecret
    scudcloud
    ntfs3g
    parted
    # skype
    graphviz
    dunst
    ranger
    bup
    lmms
    libjack2
    # anki
    freemind
    # insync

    ledger
    ledger-web

    # jb IDEs
    jetbrains.idea-community
    jetbrains.clion
    jetbrains.pycharm-community

    # Haskell env
    cabal-install
    (haskellPackages.ghcWithPackages (p: with p; [
      # build/development
      ghc
      Cabal
      hlint
      apply-refact
      # hindent
      happy
      stylish-haskell
      # haddock
      hasktags

      # languages
      Agda

      # environment
      turtle
      xmonad
      xmonad-contrib
      xmonad-extras
    ]))

    # Python env
    (python3.withPackages (pkgs: with pkgs; [
      virtualenv
      numpy
      scipy
      matplotlib
      seaborn
      pandas
      jupyter
      nbformat
      dbus
    ]))
    conda

    # Idris
    # idris

    # Prolog
    # swiProlog

    # Rust
    rustc
    cargo

    # OCaml
    opam
    ocaml
    m4

    # Julia
    # julia

    # TeX
    (texlive.combine {
      inherit (texlive)
        scheme-small
        biber
        collection-langcyrillic
        collection-xetex
        csquotes
        tabu
        varwidth
        floatrow
        algorithms
        algorithmicx
        enumitem
        biblatex
        biblatex-gost
        logreq
        xstring
        lastpage
        totcount
        chngcntr
        titlesec
        paratype
        was
        filecontents
        cm-super;
    })
    biber
    auctex
    mendeley

    mosh
    tmux
    nmap
    bind
    wireguard-tools
    discord

    glibc
    gcc
    gdb
    gnumake
    git
    tig
    global
    go
    gopls
    php
    valgrind
    pkg-config
    silver-searcher
    gitAndTools.gitflow
    # wesnoth
    # lutris

    direnv
    dmenu
    # udisks_glue
    feh
    xclip
    haskellPackages.xmobar
    xorg.xmodmap
    xorg.xkbprint
    xorg.xev
    powertop
    alsaUtils

    spotify
    
    # Vanta
    vanta-agent
  ];

  # Enable backlight management
  programs.light.enable = true;

  # Android debug bridge
  programs.adb.enable = true;

  # Services
  services = {
    # Vanta agent
    vanta-agent.enable = true; 
  
    # Disable suspend on lid close
    logind.lidSwitch = "ignore";

    # Enable fwupd
    fwupd.enable = true;

    # Enable Lorri
    lorri.enable = true;

    # Enable the OpenSSH daemon.
    openssh.enable = true;
  
    # OpenVPN
    openvpn.servers = {
      deepcake = {
        config = "config /home/flyingleafe/deepcake/client-config.ovpn";
        updateResolvConf = true;
      };
      
      qmul-eecs = {
        config = "config /home/flyingleafe/dotfiles/qmul-eecs.ovpn";
        updateResolvConf = true;
        autoStart = false;
      };
    };
   
    # Cron 
    cron = {
      enable = true;
      systemCronJobs = [
        "0 */2 * * * flyingleafe bup save -n org /home/flyingleafe/Dropbox/Org > /tmp/orgbup 2> /tmp/orgbup.err"
      ];
    }; 

    # Tor
    tor = {
      enable = true;
      client.enable = true;
      torsocks.enable = true;
    };
    
    # Postgres
    # postgresql = {
    #  enable = true;
    #  package = pkgs.postgresql;
    #  dataDir = "/var/db/postgresql/13.6";
    #  authentication = "local all all ident";
    # };
  
    # Mongo 
    mongodb = {
      enable = false;
    };


    # Enable CUPS to print documents.
    printing = {
      enable = true;
      drivers = [ pkgs.hplipWithPlugin ];
    };
   
    # Gnome keyring
    gnome.gnome-keyring.enable = true;
    
    # DBus
    dbus.enable = true;

    # Disable libinput to enable Synaptics
    libinput.enable = false;

    # X server settings
    xserver = {
      enable = true;
      
      xkb = {
          layout = "us,ru";
          options = "grp:caps_toggle";
      };

      windowManager.xmonad = {
	enable = true;
	enableContribAndExtras = true;
      };

      synaptics = {
	enable = true;
	twoFingerScroll = true;
      };

      displayManager.lightdm.enable = true;

      xautolock = {
	enable = false;
	enableNotifier = true;
	extraOptions = [ "-detectsleep" ];
	locker = "/home/flyingleafe/dotfiles/scripts/lock.sh";
	notifier = "${pkgs.libnotify}/bin/notify-send \"locking in 10 sec\"";
	time = 120;
      };

      #deviceSection = ''
      #  Option "Backlight" "intel_backlight"
      #'';
    };

  };
  
  # Enable Flatpak
  # services.flatpak.enable = true;
  # xdg.portal.enable = true;

  # Virtual LXD container
  virtualisation.lxd.enable = true;

  # Enable SANE to scan documents
  hardware.sane = {
    enable = true;
    extraBackends = [ pkgs.hplipWithPlugin ];
  };

  # Enable bluetooth
  hardware.bluetooth.enable = true;

  services.acpid.enable = true;
  # services.thinkfan.enable = true;

  services.nginx = {
    enable = true;
    httpConfig = ''
    server {
        listen 80;

        server_name multi-educator.disciplina.io;

        location / {
            proxy_pass http://localhost:3000/;
        }
    }
    '';
  };
 
  # Battery notifier/suspend
  services.batteryNotifier = {
    enable = true;
    device = "BAT1";
    notifyCapacity = 10;
    suspendCapacity = 5;
  };

  # Docker containers
  virtualisation.docker = {
    enable = true;
  };

  security.polkit.extraConfig = ''
    polkit.addRule(function(action, subject) {
      var YES = polkit.Result.YES;
      // NOTE: there must be a comma at the end of each line except for the last:
      var permission = {
        // required for udisks1:
        "org.freedesktop.udisks.filesystem-mount": YES,
        "org.freedesktop.udisks.luks-unlock": YES,
        "org.freedesktop.udisks.drive-eject": YES,
        "org.freedesktop.udisks.drive-detach": YES,
        // required for udisks2:
        "org.freedesktop.udisks2.filesystem-mount": YES,
        "org.freedesktop.udisks2.encrypted-unlock": YES,
        "org.freedesktop.udisks2.eject-media": YES,
        "org.freedesktop.udisks2.power-off-drive": YES,
        // required for udisks2 if using udiskie from another seat (e.g. systemd):
        "org.freedesktop.udisks2.filesystem-mount-other-seat": YES,
        "org.freedesktop.udisks2.filesystem-unmount-others": YES,
        "org.freedesktop.udisks2.encrypted-unlock-other-seat": YES,
        "org.freedesktop.udisks2.eject-media-other-seat": YES,
        "org.freedesktop.udisks2.power-off-drive-other-seat": YES
      };
      if (subject.isInGroup("wheel")) {
        return permission[action.id];
      }
    });
  '';

  security.pki.certificates = [
    (builtins.readFile /home/flyingleafe/certs/myCA.crt)
  ];

  security.wrappers = {
    dumpcap = {
      program = "dumpcap";
      source = "${pkgs.wireshark}/bin/dumpcap";
      owner = "root";
      group = "wireshark";
      setuid = true;
      setgid = false;
      permissions = "u+rx,g+x";
    };
  };

  security.pam.loginLimits = [
    { domain = "*";
      type   = "soft";
      item   = "core";
      value  = "512";
    }
    { domain = "*";
      type   = "hard";
      item   = "core";
      value  = "4096";
    }
  ];

  systemd.tmpfiles.rules = [
    "L /etc/vanta.conf - - - - /root/.vanta/vanta.conf"
  ];
 
  systemd.coredump.enable = true;

  systemd.user.services.udiskie = {
    enable = true;
    description = "Removable disk automounter";
    wantedBy = [ "default.target" ];
    path = with pkgs; [
      # gnome3.defaultIconTheme
      # gnome3.gnome_themes_standard
      udiskie
    ];
    # environment.XDG_DATA_DIRS="${pkgs.gnome3.defaultIconTheme}/share:${pkgs.gnome3.gnome_themes_standard}/share";
    serviceConfig = {
      Restart = "always";
      ExecStart = "${pkgs.udiskie}/bin/udiskie --automount --notify --tray --use-udisks2";
    };
  };

  systemd.user.services.dropbox = {
    enable = true;
    description = "Dropbox daemon";
    wantedBy = [ "default.target" ];
    path = with pkgs; [
      dropbox-cli
    ];
    serviceConfig = {
      Restart = "always";
      Type = "forking";
      ExecStart = "${pkgs.dropbox-cli}/bin/dropbox start";
      ExecStop = "${pkgs.dropbox-cli}/bin/dropbox stop";
    };
  };

  # GPG
  programs.gnupg.agent = {
    enable = true;
  };

  # ZSH
  programs.zsh.enable = true;
  users.defaultUserShell = "/run/current-system/sw/bin/zsh";

  users.extraGroups.wireshark.gid = 500;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.extraUsers = {
    flyingleafe = {
      isNormalUser = true;
      home = "/home/flyingleafe";
      description = "Dmitry Mukhutdinov";
      extraGroups = [ "wheel" "networkmanager" "wireshark" "video" "docker" "lxd" "adbusers" "kvm" ];
      shell = "/run/current-system/sw/bin/zsh";
    };

    hbb228 = {
      isNormalUser = true;
      home = "/home/hbb228";
      description = "hbb228";
      extraGroups = [ "wheel" "networkmanager" "wireshark" "video" "docker" "lxd" ];
    };
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "20.03"; # Did you read the comment?

}
