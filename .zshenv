# User configuration

typeset -U path
path=(~/go/bin ~/bin $path)
export GOPATH="$HOME/go"
export VK_ENGINES_DIR=$HOME/VK/engine-ro
