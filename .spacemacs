;; -*- mode: dotspacemacs -*-
;; This file is loaded by Spacemacs at startup.
;; It must be stored in your home directory.

(defun dotspacemacs/layers ()
  "Configuration Layers declaration.
You should not put any user code in this function besides modifying the variable
values."
  (setq-default
   ;; Base distribution to use. This is a layer contained in the directory
   ;; `+distribution'. For now available distributions are `spacemacs-base'
   ;; or `spacemacs'. (default 'spacemacs)
   dotspacemacs-distribution 'spacemacs
   ;; Lazy installation of layers (i.e. layers are installed only when a file
   ;; with a supported type is opened). Possible values are `all', `unused'
   ;; and `nil'. `unused' will lazy install only unused layers (i.e. layers
   ;; not listed in variable `dotspacemacs-configuration-layers'), `all' will
   ;; lazy install any layer that support lazy installation even the layers
   ;; listed in `dotspacemacs-configuration-layers'. `nil' disable the lazy
   ;; installation feature and you have to explicitly list a layer in the
   ;; variable `dotspacemacs-configuration-layers' to install it.
   ;; (default 'unused)
   dotspacemacs-enable-lazy-installation 'unused
   ;; If non-nil then Spacemacs will ask for confirmation before installing
   ;; a layer lazily. (default t)
   dotspacemacs-ask-for-lazy-installation t
   ;; List of additional paths where to look for configuration layers.
   ;; Paths must have a trailing slash (i.e. `~/.mycontribs/')
   dotspacemacs-configuration-layer-path '()
   ;; List of configuration layers to load.
   dotspacemacs-configuration-layers
   '(
     csv
     typescript
     idris
     elixir
     graphviz
     purescript
     nginx
     python
     sql
     yaml
     ;; ----------------------------------------------------------------
     ;; Example of useful layers you may want to use right away.
     ;; Uncomment some layer names and press <SPC f e R> (Vim style) or
     ;; <M-m f e R> (Emacs style) to install them.
     ;; ----------------------------------------------------------------
     ;; auto-completion
     ;; better-defaults
     emacs-lisp
     git
     markdown
     (org :variables
          org-enable-org-journal-support t
          org-enable-roam-support t
          org-enable-reveal-js-support t)
     finance
     ;; (shell :variables
     ;;        shell-default-height 30
     ;;        shell-default-position 'bottom)
     ;; spell-checking
     ;; syntax-checking
     ;; version-control
     (haskell :variables
              haskell-completion-backend 'lsp)
     lsp
     ocaml
     nixos
     ;; vim-powerline
     gtags
     (go :variables
         go-tab-width 2)
     php
     lua
     javascript
     html
     c-c++
     erlang
     auto-completion
     syntax-checking
     )
   ;; List of additional packages that will be installed without being
   ;; wrapped in a layer. If you need some configuration for these
   ;; packages, then consider creating a layer. You can also put the
   ;; configuration in `dotspacemacs/user-config'.
   dotspacemacs-additional-packages
   '(
     org-clock-convenience
     org-habit
     direnv
     )
   ;; A list of packages and/or extensions that will not be install and loaded.
   dotspacemacs-excluded-packages '()
   ;; Defines the behaviour of Spacemacs when installing packages.
   ;; Possible values are `used-only', `used-but-keep-unused' and `all'.
   ;; `used-only' installs only explicitly used packages and uninstall any
   ;; unused packages as well as their unused dependencies.
   ;; `used-but-keep-unused' installs only the used packages but won't uninstall
   ;; them if they become unused. `all' installs *all* packages supported by
   ;; Spacemacs and never uninstall them. (default is `used-only')
   dotspacemacs-install-packages 'used-only))

(defun dotspacemacs/init ()
  "Initialization function.
This function is called at the very startup of Spacemacs initialization
before layers configuration.
You should not put any user code in there besides modifying the variable
values."
  ;; This setq-default sexp is an exhaustive list of all the supported
  ;; spacemacs settings.
  (setq-default
   ;; If non nil ELPA repositories are contacted via HTTPS whenever it's
   ;; possible. Set it to nil if you have no way to use HTTPS in your
   ;; environment, otherwise it is strongly recommended to let it set to t.
   ;; This variable has no effect if Emacs is launched with the parameter
   ;; `--insecure' which forces the value of this variable to nil.
   ;; (default t)
   dotspacemacs-elpa-https t
   ;; Maximum allowed time in seconds to contact an ELPA repository.
   dotspacemacs-elpa-timeout 5
   ;; If non nil then spacemacs will check for updates at startup
   ;; when the current branch is not `develop'. (default t)
   dotspacemacs-check-for-update t
   ;; One of `vim', `emacs' or `hybrid'. Evil is always enabled but if the
   ;; variable is `emacs' then the `holy-mode' is enabled at startup. `hybrid'
   ;; uses emacs key bindings for vim's insert mode, but otherwise leaves evil
   ;; unchanged. (default 'vim)
   dotspacemacs-editing-style 'vim
   ;; If non nil output loading progress in `*Messages*' buffer. (default nil)
   dotspacemacs-verbose-loading nil
   ;; Specify the startup banner. Default value is `official', it displays
   ;; the official spacemacs logo. An integer value is the index of text
   ;; banner, `random' chooses a random text banner in `core/banners'
   ;; directory. A string value must be a path to an image format supported
   ;; by your Emacs build.
   ;; If the value is nil then no banner is displayed. (default 'official)
   dotspacemacs-startup-banner 'official
   ;; List of items to show in the startup buffer. If nil it is disabled.
   ;; Possible values are: `recents' `bookmarks' `projects'.
   ;; (default '(recents projects))
   dotspacemacs-startup-lists '((recents . 5)
                                (agenda . 7))
   ;; True if the home buffer should respond to resize events.
   dotspacemacs-startup-buffer-responsive t
   ;; Default major mode of the scratch buffer (default `text-mode')
   dotspacemacs-scratch-mode 'text-mode
   ;; List of themes, the first of the list is loaded when spacemacs starts.
   ;; Press <SPC> T n to cycle to the next theme in the list (works great
   ;; with 2 themes variants, one dark and one light)
   dotspacemacs-themes '(spacemacs-dark
                         spacemacs-light
                         solarized-light
                         solarized-dark
                         leuven
                         monokai
                         zenburn)
   ;; If non nil the cursor color matches the state color in GUI Emacs.
   dotspacemacs-colorize-cursor-according-to-state t
   ;; Default font. `powerline-scale' allows to quickly tweak the mode-line
   ;; size to make separators look not too crappy.
   dotspacemacs-default-font '("Source Code Pro"
                               :size 18
                               :weight normal
                               :width normal
                               :powerline-scale 1.2)
   ;; The leader key
   dotspacemacs-leader-key "SPC"
   ;; The key used for Emacs commands (M-x) (after pressing on the leader key).
   ;; (default "SPC")
   dotspacemacs-emacs-command-key "SPC"
   ;; The key used for Vim Ex commands (default ":")
   dotspacemacs-ex-command-key ":"
   ;; The leader key accessible in `emacs state' and `insert state'
   ;; (default "M-m")
   dotspacemacs-emacs-leader-key "M-m"
   ;; Major mode leader key is a shortcut key which is the equivalent of
   ;; pressing `<leader> m`. Set it to `nil` to disable it. (default ",")
   dotspacemacs-major-mode-leader-key ","
   ;; Major mode leader key accessible in `emacs state' and `insert state'.
   ;; (default "C-M-m)
   dotspacemacs-major-mode-emacs-leader-key "C-M-m"
   ;; These variables control whether separate commands are bound in the GUI to
   ;; the key pairs C-i, TAB and C-m, RET.
   ;; Setting it to a non-nil value, allows for separate commands under <C-i>
   ;; and TAB or <C-m> and RET.
   ;; In the terminal, these pairs are generally indistinguishable, so this only
   ;; works in the GUI. (default nil)
   dotspacemacs-distinguish-gui-tab nil
    ;; If non nil `Y' is remapped to `y$'. (default t)
   dotspacemacs-remap-Y-to-y$ t
   ;; If non-nil, the shift mappings `<' and `>' retain visual state if used
   ;; there. (default t)
   dotspacemacs-retain-visual-state-on-shift t
   ;; If non-nil, J and K move lines up and down when in visual mode.
   ;; (default nil)
   dotspacemacs-visual-line-move-text nil
   ;; If non nil, inverse the meaning of `g' in `:substitute' Evil ex-command.
   ;; (default nil)
   dotspacemacs-ex-substitute-global nil
   ;; Name of the default layout (default "Default")
   dotspacemacs-default-layout-name "Default"
   ;; If non nil the default layout name is displayed in the mode-line.
   ;; (default nil)
   dotspacemacs-display-default-layout nil
   ;; If non nil then the last auto saved layouts are resume automatically upon
   ;; start. (default nil)
   dotspacemacs-auto-resume-layouts nil
   ;; Size (in MB) above which spacemacs will prompt to open the large file
   ;; literally to avoid performance issues. Opening a file literally means that
   ;; no major mode or minor modes are active. (default is 1)
   dotspacemacs-large-file-size 1
   ;; Location where to auto-save files. Possible values are `original' to
   ;; auto-save the file in-place, `cache' to auto-save the file to another
   ;; file stored in the cache directory and `nil' to disable auto-saving.
   ;; (default 'cache)
   dotspacemacs-auto-save-file-location 'cache
   ;; Maximum number of rollback slots to keep in the cache. (default 5)
   dotspacemacs-max-rollback-slots 5
   ;; If non nil then `ido' replaces `helm' for some commands. For now only
   ;; `find-files' (SPC f f), `find-spacemacs-file' (SPC f e s), and
   ;; `find-contrib-file' (SPC f e c) are replaced. (default nil)
   dotspacemacs-use-ido nil
   ;; If non nil, `helm' will try to minimize the space it uses. (default nil)
   dotspacemacs-helm-resize nil
   ;; if non nil, the helm header is hidden when there is only one source.
   ;; (default nil)
   dotspacemacs-helm-no-header nil
   ;; define the position to display `helm', options are `bottom', `top',
   ;; `left', or `right'. (default 'bottom)
   dotspacemacs-helm-position 'bottom
   ;; Controls fuzzy matching in helm. If set to `always', force fuzzy matching
   ;; in all non-asynchronous sources. If set to `source', preserve individual
   ;; source settings. Else, disable fuzzy matching in all sources.
   ;; (default 'always)
   dotspacemacs-helm-use-fuzzy 'always
   ;; If non nil the paste micro-state is enabled. When enabled pressing `p`
   ;; several times cycle between the kill ring content. (default nil)
   dotspacemacs-enable-paste-micro-state nil
   ;; Which-key delay in seconds. The which-key buffer is the popup listing
   ;; the commands bound to the current keystroke sequence. (default 0.4)
   dotspacemacs-which-key-delay 0.4
   ;; Which-key frame position. Possible values are `right', `bottom' and
   ;; `right-then-bottom'. right-then-bottom tries to display the frame to the
   ;; right; if there is insufficient space it displays it at the bottom.
   ;; (default 'bottom)
   dotspacemacs-which-key-position 'bottom
   ;; If non nil a progress bar is displayed when spacemacs is loading. This
   ;; may increase the boot time on some systems and emacs builds, set it to
   ;; nil to boost the loading time. (default t)
   dotspacemacs-loading-progress-bar t
   ;; If non nil the frame is fullscreen when Emacs starts up. (default nil)
   ;; (Emacs 24.4+ only)
   dotspacemacs-fullscreen-at-startup nil
   ;; If non nil `spacemacs/toggle-fullscreen' will not use native fullscreen.
   ;; Use to disable fullscreen animations in OSX. (default nil)
   dotspacemacs-fullscreen-use-non-native nil
   ;; If non nil the frame is maximized when Emacs starts up.
   ;; Takes effect only if `dotspacemacs-fullscreen-at-startup' is nil.
   ;; (default nil) (Emacs 24.4+ only)
   dotspacemacs-maximized-at-startup nil
   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's active or selected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-active-transparency 90
   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's inactive or deselected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-inactive-transparency 90
   ;; If non nil show the titles of transient states. (default t)
   dotspacemacs-show-transient-state-title t
   ;; If non nil show the color guide hint for transient state keys. (default t)
   dotspacemacs-show-transient-state-color-guide t
   ;; If non nil unicode symbols are displayed in the mode line. (default t)
   dotspacemacs-mode-line-unicode-symbols t
   ;; If non nil smooth scrolling (native-scrolling) is enabled. Smooth
   ;; scrolling overrides the default behavior of Emacs which recenters the
   ;; point when it reaches the top or bottom of the screen. (default t)
   dotspacemacs-smooth-scrolling t
   ;; Control line numbers activation.
   ;; If set to `t' or `relative' line numbers are turned on in all `prog-mode' and
   ;; `text-mode' derivatives. If set to `relative', line numbers are relative.
   ;; This variable can also be set to a property list for finer control:
   ;; '(:relative nil
   ;;   :disabled-for-modes dired-mode
   ;;                       doc-view-mode
   ;;                       markdown-mode
   ;;                       org-mode
   ;;                       pdf-view-mode
   ;;                       text-mode
   ;;   :size-limit-kb 1000)
   ;; (default nil)
   dotspacemacs-line-numbers nil
   ;; Code folding method. Possible values are `evil' and `origami'.
   ;; (default 'evil)
   dotspacemacs-folding-method 'evil
   ;; If non-nil smartparens-strict-mode will be enabled in programming modes.
   ;; (default nil)
   dotspacemacs-smartparens-strict-mode nil
   ;; If non-nil pressing the closing parenthesis `)' key in insert mode passes
   ;; over any automatically added closing parenthesis, bracket, quote, etc…
   ;; This can be temporary disabled by pressing `C-q' before `)'. (default nil)
   dotspacemacs-smart-closing-parenthesis nil
   ;; Select a scope to highlight delimiters. Possible values are `any',
   ;; `current', `all' or `nil'. Default is `all' (highlight any scope and
   ;; emphasis the current one). (default 'all)
   dotspacemacs-highlight-delimiters 'all
   ;; If non nil advises quit functions to keep server open when quitting.
   ;; (default nil)
   dotspacemacs-persistent-server nil
   ;; List of search tool executable names. Spacemacs uses the first installed
   ;; tool of the list. Supported tools are `ag', `pt', `ack' and `grep'.
   ;; (default '("ag" "pt" "ack" "grep"))
   dotspacemacs-search-tools '("ag" "pt" "ack" "grep")
   ;; The default package repository used if no explicit repository has been
   ;; specified with an installed package.
   ;; Not used for now. (default nil)
   dotspacemacs-default-package-repository nil
   ;; Delete whitespace while saving buffer. Possible values are `all'
   ;; to aggressively delete empty line and long sequences of whitespace,
   ;; `trailing' to delete only the whitespace at end of lines, `changed'to
   ;; delete only whitespace for changed lines or `nil' to disable cleanup.
   ;; (default nil)
   dotspacemacs-whitespace-cleanup 'all
   ))

(defun dotspacemacs/user-init ()
  "Initialization function for user code.
It is called immediately after `dotspacemacs/init'.  You are free to put almost
any user code here.  The exception is org related code, which should be placed
in `dotspacemacs/user-config'."
  )

(defun dotspacemacs/user-config ()
  "Configuration function for user code.
This function is called at the very end of Spacemacs initialization after
layers configuration. You are free to put any user code."

  (defun add-to-hooks (mode hooks)
    (dolist (hook hooks)
      (add-hook 'hook (lambda () (mode)))))

  ;; Line move
  (defun move-line-up ()
    (interactive)
    (transpose-lines 1)
    (forward-line -2))

  (defun move-line-down ()
    (interactive)
    (forward-line 1)
    (transpose-lines 1)
    (forward-line -1))

  (global-set-key (kbd "<M-up>") 'move-line-up)
  (global-set-key (kbd "<M-down>") 'move-line-down)

  ;; Windmove
  (global-set-key (kbd "<s-up>") 'windmove-up)
  (global-set-key (kbd "<s-down>") 'windmove-down)
  (global-set-key (kbd "<s-left>") 'windmove-left)
  (global-set-key (kbd "<s-right>") 'windmove-right)

  ;; Line Numbers
  (add-to-hooks 'linum-mode '(php-mode-hook
                              js2-mode-hook
                              c-mode-hook
                              c++-mode-hook
                              haskell-mode-hook
                              python-mode-hook))

  ;; Which function
  (which-function-mode 1)

  ;; Nerd Commenter
  (evil-leader/set-key "\\" 'evilnc-comment-or-uncomment-lines)

  ;; Yasnippet
  (global-set-key (kbd "<C-return>") 'yas-expand)

  ;; Want to indent cases everywhere
  (c-set-offset 'case-label '+)

  ;; In Go (for example), I use tabs, and I need them to be neat
  (setq tab-width 2)

  ;; Remove trailing whitespace automatically and set trailing newlines
  (setq require-final-newline t)
  (setq-default require-final-newline t)

  (setq enable-remote-dir-locals t)
  (setq password-cache-expiry nil)


  ;; Direnv ;;

  (direnv-mode)

  ;; Org mode ;;
  (with-eval-after-load 'org

    ;; Habits tracking
    (add-to-list 'org-modules "org-habits")
    (setq org-habit-graph-column 52)

    ;; Drill
    ;; (require 'org-drill)
    ;; (add-to-list 'org-modules "org-drill")

    ;; Folders and files definition
    (setq org-directory (expand-file-name "~/Dropbox/Org"))
    (defvar flf-todo-folder  (format "%s/%s" org-directory "Todo"))
    (defvar flf-main-todo    (format "%s/%s" flf-todo-folder "HotTodo.org.gpg"))
    (defvar flf-serokell     (format "%s/%s" flf-todo-folder "Serokell.org.gpg"))
    (defvar flf-research     (format "%s/%s" flf-todo-folder "Research.org.gpg"))
    (defvar flf-habits       (format "%s/%s" flf-todo-folder "Habits.org.gpg"))
    (defvar flf-schedule     (format "%s/%s" org-directory "Schedule.org.gpg"))
    (defvar flf-notes        (format "%s/%s" org-directory "Notes.org.gpg"))
    (defvar flf-times        (format "%s/%s" org-directory "Times.org.gpg"))
    (defvar flf-diary-folder (format "%s/%s" org-directory "Diary"))
    (defvar flf-clock-file   (format "%s/%s" org-directory ".org-clock-persist.el"))
    (defvar flf-ledger       (format "%s/%s" org-directory "journal.ledger.gpg")) ;; not an Org file, but still

    ;; Agenda setup
    (setq org-agenda-files (list
                            flf-serokell
                            flf-research
                            flf-main-todo
                            flf-habits
                            flf-schedule
                            flf-times))

    (use-package org-clock-convenience
      :ensure t
      :bind (:map org-agenda-mode-map
                  ("<S-up>" . org-clock-convenience-timestamp-up)
                  ("<S-down>" . org-clock-convenience-timestamp-down)
                  ("C-c `" . org-clock-convenience-fill-gap)
                  ("C-c ~" . org-clock-convenience-fill-gap-both)))

    (setq org-agenda-skip-scheduled-if-done t)
    (setq org-agenda-skip-deadline-if-done t)

    ;; Keybindings
    (defmacro declare-file-opener (funcname filename)
      `(defun ,funcname ()
         (interactive)
         (find-file ,filename)))

    (declare-file-opener flf-open-main-todo flf-main-todo)
    (declare-file-opener flf-open-habits flf-habits)
    (declare-file-opener flf-open-schedule flf-schedule)
    (declare-file-opener flf-open-notes flf-notes)
    (declare-file-opener flf-open-times flf-times)
    (declare-file-opener flf-open-serokell flf-serokell)
    (declare-file-opener flf-open-research flf-research)
    (declare-file-opener flf-open-ledger flf-ledger)

    (spacemacs/declare-prefix "o" "org-custom-prefix")
    (spacemacs/set-leader-keys "o m" 'flf-open-main-todo)
    (spacemacs/set-leader-keys "o k" 'flf-open-serokell)
    (spacemacs/set-leader-keys "o r" 'flf-open-research)
    (spacemacs/set-leader-keys "o h" 'flf-open-habits)
    (spacemacs/set-leader-keys "o s" 'flf-open-schedule)
    (spacemacs/set-leader-keys "o n" 'flf-open-notes)
    (spacemacs/set-leader-keys "o t" 'flf-open-times)
    (spacemacs/set-leader-keys "o a" 'org-agenda)
    (spacemacs/set-leader-keys "o l" 'flf-open-ledger)

    ;; Better clocking keybindings
    (spacemacs/set-leader-keys "m I" 'org-clock-in)
    (spacemacs/set-leader-keys "m O" 'org-clock-out)

    ;; Org-journal
    (setq org-journal-dir flf-diary-folder
          org-journal-encrypt-journal t
          org-journal-file-format "%Y-%m-%d.org")

    (spacemacs/declare-prefix "o j" "org journal")
    (spacemacs/set-leader-keys "o j j" 'org-journal-new-entry)

    ;; todo dependencies
    (setq org-enforce-todo-dependencies t)

    ;; clocking
    (setq org-clock-persist t)
    (setq org-clock-persist-file flf-clock-file)
    (org-clock-persistence-insinuate)

    ;; auto-save advices
    (add-hook 'org-mode-hook 'auto-fill-mode)
    (add-hook 'org-clock-in-hook 'org-save-all-org-buffers)
    (add-hook 'org-clock-out-hook 'org-save-all-org-buffers)
    (add-hook 'org-clock-cancel-hook 'org-save-all-org-buffers)
  )

  ;; Haskell mode ;;

  ;; stylish-haskell auto-apply
  ;; (add-hook 'haskell-mode-hook
  ;;           '(lambda () (add-hook 'before-save-hook 'haskell-mode-stylish-buffer)))

  ;; Misc ;;

  ;; font resizing
  (spacemacs/set-leader-keys "m -" 'spacemacs/scale-down-font)
  (spacemacs/set-leader-keys "m +" 'spacemacs/scale-up-font)
  )

;; Do not write anything past this comment. This is where Emacs will
;; auto-generate custom variable definitions.
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auth-source-save-behavior nil)
 '(css-indent-offset 2)
 '(js-indent-level 2)
 '(org-agenda-log-mode-items '(clock))
 '(org-log-into-drawer t)
 '(org-reveal-title-slide "<h3 class='title'>%t</h3><h4 class='author'>%a</h4>")
 '(package-selected-packages
   '(direnv org-roam magit-section emacsql-sqlite emacsql csv-mode tide typescript-mode lv transient idris-mode prop-menu ob-elixir flycheck-mix flycheck-credo alchemist elixir-mode vue-html-mode winum org-mime fuzzy powerline spinner org org-category-capture alert log4e gntp markdown-mode skewer-mode simple-httpd json-snatcher json-reformat multiple-cursors js2-mode hydra parent-mode projectile request haml-mode gitignore-mode pos-tip flycheck pkg-info epl flx magit magit-popup git-commit ghub treepy graphql with-editor smartparens iedit anzu evil goto-chg highlight diminish web-completion-data dash-functional tern nixos-options go-mode haskell-mode company bind-map bind-key yasnippet packed anaconda-mode pythonic helm avy helm-core async auto-complete popup f s dash php-mode utop tuareg caml ocp-indent merlin ledger-mode flycheck-ledger org-clock-convenience erlang graphviz-dot-mode hide-comnt go-guru psci purescript-mode psc-ide nginx-mode flycheck-haskell company-ghci company-ghc ghc hlint-refactor hindent helm-hoogle haskell-snippets company-cabal cmm-mode yapfify yaml-mode ws-butler window-numbering which-key web-mode web-beautify volatile-highlights vi-tilde-fringe uuidgen use-package toc-org tagedit sql-indent spacemacs-theme spaceline smeargle slim-mode scss-mode sass-mode restart-emacs rainbow-delimiters quelpa pyvenv pytest pyenv-mode py-isort pug-mode popwin pip-requirements phpunit phpcbf php-extras php-auto-yasnippets persp-mode pcre2el paradox ox-reveal orgit org-projectile org-present org-pomodoro org-plus-contrib org-journal org-download org-bullets open-junk-file nix-mode neotree move-text mmm-mode markdown-toc magit-gitflow macrostep lua-mode lorem-ipsum livid-mode live-py-mode linum-relative link-hint less-css-mode json-mode js2-refactor js-doc intero info+ indent-guide ido-vertical-mode hy-mode hungry-delete htmlize hl-todo highlight-parentheses highlight-numbers highlight-indentation help-fns+ helm-themes helm-swoop helm-pydoc helm-projectile helm-nixos-options helm-mode-manager helm-make helm-gtags helm-gitignore helm-flx helm-descbinds helm-css-scss helm-company helm-c-yasnippet helm-ag google-translate golden-ratio go-eldoc gnuplot gitconfig-mode gitattributes-mode git-timemachine git-messenger git-link gh-md ggtags flycheck-pos-tip flx-ido fill-column-indicator fancy-battery eyebrowse expand-region exec-path-from-shell evil-visualstar evil-visual-mark-mode evil-unimpaired evil-tutor evil-surround evil-search-highlight-persist evil-numbers evil-nerd-commenter evil-mc evil-matchit evil-magit evil-lisp-state evil-indent-plus evil-iedit-state evil-exchange evil-escape evil-ediff evil-args evil-anzu eval-sexp-fu emmet-mode elisp-slime-nav dumb-jump drupal-mode disaster define-word cython-mode company-web company-tern company-statistics company-nixos-options company-go company-c-headers company-anaconda column-enforce-mode coffee-mode cmake-mode clean-aindent-mode clang-format auto-yasnippet auto-highlight-symbol auto-compile aggressive-indent adaptive-wrap ace-window ace-link ace-jump-helm-line ac-ispell))
 '(require-final-newline t)
 '(safe-local-variable-values
   '((haskell-process-use-ghci . t)
     (haskell-indent-spaces . 4)
     (haskell-indent-offset . 4)
     (haskell-indentation-left-offset . 4)
     (haskell-indentation-layout-offset . 4)
     (haskell-indentation-starter-offset . 4)
     (haskell-indentation-where-pre-offset . 2)
     (haskell-indentation-where-post-offset . 2)
     (eval let*
           ((src-dir
             (expand-file-name "~/VK/c_dev_repo/nginx/nginx_src"))
            (mapper
             (lambda
               (dir)
               (format "%s/%s" src-dir dir)))
            (inc-dirs
             '("src/core" "src/event" "src/event/modules" "src/os/unix" "objs" "src/http" "src/http/modules" "src/mail" "src/stream"))
            (full-inc-dirs
             (mapcar mapper inc-dirs)))
           (setq flycheck-clang-include-path full-inc-dirs)
           (setq flycheck-gcc-include-path full-inc-dirs))
     (c-default-style . "linux")
     (eval let*
           ((cur-dir
             (projectile-project-root))
            (src-list
             (file-expand-wildcards
              (concat cur-dir "nginx/nginx_src/src/*")))
            (objs-folder
             (concat cur-dir "nginx/nginx_src/objs")))
           (progn
             (setq flycheck-clang-include-path
                   (add-to-list 'src-list objs-folder))))
     (eval progn
           (add-to-list 'file-coding-system-alist
                        '("\\.php\\'" cp1251 . cp1251))
           (add-to-list 'auto-coding-alist
                        '("\\.php\\'" . cp1251))
           (c-set-offset 'case-label '+))
     (eval progn
           (add-to-list 'file-coding-system-alist
                        '("\\.php\\'" cp1251 . cp1251))
           (add-to-list 'auto-coding-alist
                        '("\\.php\\'" . cp1251)))))
 '(tide-format-options '(:indentSize 2 :tabSize 2))
 '(typescript-indent-level 2)
 '(web-mode-code-indent-offset 2)
 '(web-mode-indent-style 2)
 '(web-mode-markup-indent-offset 2))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-tooltip-common ((t (:inherit company-tooltip :weight bold :underline nil))))
 '(company-tooltip-common-selection ((t (:inherit company-tooltip-selection :weight bold :underline nil)))))
(defun dotspacemacs/emacs-custom-settings ()
  "Emacs custom settings.
This is an auto-generated function, do not modify its content directly, use
Emacs customize menu instead.
This function is called at the very end of Spacemacs initialization."
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auth-source-save-behavior nil)
 '(css-indent-offset 2)
 '(evil-want-Y-yank-to-eol t)
 '(haskell-indentation-layout-offset 2)
 '(js-indent-level 2)
 '(org-agenda-log-mode-items '(clock))
 '(org-log-into-drawer t)
 '(org-reveal-title-slide "<h3 class='title'>%t</h3><h4 class='author'>%a</h4>")
 '(package-selected-packages
   '(solidity-mode dap-mode bui direnv org-roam magit-section emacsql-sqlite emacsql csv-mode tide typescript-mode lv transient idris-mode prop-menu ob-elixir flycheck-mix flycheck-credo alchemist elixir-mode vue-html-mode winum org-mime fuzzy powerline spinner org org-category-capture alert log4e gntp markdown-mode skewer-mode simple-httpd json-snatcher json-reformat multiple-cursors js2-mode hydra parent-mode projectile request haml-mode gitignore-mode pos-tip flycheck pkg-info epl flx magit magit-popup git-commit ghub treepy graphql with-editor smartparens iedit anzu evil goto-chg highlight diminish web-completion-data dash-functional tern nixos-options go-mode haskell-mode company bind-map bind-key yasnippet packed anaconda-mode pythonic helm avy helm-core async auto-complete popup f s dash php-mode utop tuareg caml ocp-indent merlin ledger-mode flycheck-ledger org-clock-convenience erlang graphviz-dot-mode hide-comnt go-guru psci purescript-mode psc-ide nginx-mode flycheck-haskell company-ghci company-ghc ghc hlint-refactor hindent helm-hoogle haskell-snippets company-cabal cmm-mode yapfify yaml-mode ws-butler window-numbering which-key web-mode web-beautify volatile-highlights vi-tilde-fringe uuidgen use-package toc-org tagedit sql-indent spacemacs-theme spaceline smeargle slim-mode scss-mode sass-mode restart-emacs rainbow-delimiters quelpa pyvenv pytest pyenv-mode py-isort pug-mode popwin pip-requirements phpunit phpcbf php-extras php-auto-yasnippets persp-mode pcre2el paradox ox-reveal orgit org-projectile org-present org-pomodoro org-plus-contrib org-journal org-download org-bullets open-junk-file nix-mode neotree move-text mmm-mode markdown-toc magit-gitflow macrostep lua-mode lorem-ipsum livid-mode live-py-mode linum-relative link-hint less-css-mode json-mode js2-refactor js-doc intero info+ indent-guide ido-vertical-mode hy-mode hungry-delete htmlize hl-todo highlight-parentheses highlight-numbers highlight-indentation help-fns+ helm-themes helm-swoop helm-pydoc helm-projectile helm-nixos-options helm-mode-manager helm-make helm-gtags helm-gitignore helm-flx helm-descbinds helm-css-scss helm-company helm-c-yasnippet helm-ag google-translate golden-ratio go-eldoc gnuplot gitconfig-mode gitattributes-mode git-timemachine git-messenger git-link gh-md ggtags flycheck-pos-tip flx-ido fill-column-indicator fancy-battery eyebrowse expand-region exec-path-from-shell evil-visualstar evil-visual-mark-mode evil-unimpaired evil-tutor evil-surround evil-search-highlight-persist evil-numbers evil-nerd-commenter evil-mc evil-matchit evil-magit evil-lisp-state evil-indent-plus evil-iedit-state evil-exchange evil-escape evil-ediff evil-args evil-anzu eval-sexp-fu emmet-mode elisp-slime-nav dumb-jump drupal-mode disaster define-word cython-mode company-web company-tern company-statistics company-nixos-options company-go company-c-headers company-anaconda column-enforce-mode coffee-mode cmake-mode clean-aindent-mode clang-format auto-yasnippet auto-highlight-symbol auto-compile aggressive-indent adaptive-wrap ace-window ace-link ace-jump-helm-line ac-ispell))
 '(require-final-newline t)
 '(safe-local-variable-values
   '((haskell-process-use-ghci . t)
     (haskell-indent-spaces . 4)
     (haskell-indent-offset . 4)
     (haskell-indentation-left-offset . 4)
     (haskell-indentation-layout-offset . 4)
     (haskell-indentation-starter-offset . 4)
     (haskell-indentation-where-pre-offset . 2)
     (haskell-indentation-where-post-offset . 2)
     (eval let*
           ((src-dir
             (expand-file-name "~/VK/c_dev_repo/nginx/nginx_src"))
            (mapper
             (lambda
               (dir)
               (format "%s/%s" src-dir dir)))
            (inc-dirs
             '("src/core" "src/event" "src/event/modules" "src/os/unix" "objs" "src/http" "src/http/modules" "src/mail" "src/stream"))
            (full-inc-dirs
             (mapcar mapper inc-dirs)))
           (setq flycheck-clang-include-path full-inc-dirs)
           (setq flycheck-gcc-include-path full-inc-dirs))
     (c-default-style . "linux")
     (eval let*
           ((cur-dir
             (projectile-project-root))
            (src-list
             (file-expand-wildcards
              (concat cur-dir "nginx/nginx_src/src/*")))
            (objs-folder
             (concat cur-dir "nginx/nginx_src/objs")))
           (progn
             (setq flycheck-clang-include-path
                   (add-to-list 'src-list objs-folder))))
     (eval progn
           (add-to-list 'file-coding-system-alist
                        '("\\.php\\'" cp1251 . cp1251))
           (add-to-list 'auto-coding-alist
                        '("\\.php\\'" . cp1251))
           (c-set-offset 'case-label '+))
     (eval progn
           (add-to-list 'file-coding-system-alist
                        '("\\.php\\'" cp1251 . cp1251))
           (add-to-list 'auto-coding-alist
                        '("\\.php\\'" . cp1251)))))
 '(tide-format-options '(:indentSize 2 :tabSize 2))
 '(typescript-indent-level 2)
 '(web-mode-code-indent-offset 2)
 '(web-mode-indent-style 2)
 '(web-mode-markup-indent-offset 2))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-tooltip-common ((t (:inherit company-tooltip :weight bold :underline nil))))
 '(company-tooltip-common-selection ((t (:inherit company-tooltip-selection :weight bold :underline nil)))))
)
