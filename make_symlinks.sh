#/usr/bin/env bash
cur_dir=$( cd $(dirname ${BASH_SOURCE[0]}) && pwd)
xmonad_dir=$HOME/.xmonad
home_cfg=$HOME/.config
dunst_dir=$home_cfg/dunst

sys_dir=$cur_dir/system
sys_config=/etc/nixos/configuration.nix
hardware_config=/etc/nixos/hardware-configuration.nix

mkdir -p $xmonad_dir
mkdir -p $dunst_dir

ln -f -s $cur_dir/xmonad.hs $xmonad_dir/xmonad.hs
ln -f -s $cur_dir/dunstrc $dunst_dir/dunstrc

dotfiles=(
    .xinitrc
    .xmobarrc
    .Xresources
    .zprofile
    .zshrc
    .zshenv
    .spacemacs
    .gitattributes
    .gitconfig
    .vimrc
    .stylish-haskell.yaml
    .orgstat.yaml
    .gdbinit
)

for file in ${dotfiles[*]}
do
    ln -f -s $cur_dir/$file $HOME/$file
done

#sudo ln -f -s $sys_dir/configuration.nix $sys_config
#sudo ln -f -s $sys_dir/hardware-configuration.nix $hardware_config
