import Control.Monad
import Graphics.X11.ExtraTypes.XF86
import System.IO
import XMonad
import XMonad.Actions.Volume
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.SetWMName
import XMonad.Layout.IndependentScreens
import XMonad.Layout.NoBorders
import qualified XMonad.StackSet as W
import XMonad.Util.EZConfig (additionalKeys)

myModMask = mod4Mask
myWorkspaces =
        --withScreens 2 $
        map show [1..9]

myLayout = smartBorders $ layoutHook defaultConfig

myConfig = defaultConfig
     { terminal = "urxvt -ls"
     , modMask  = myModMask
     , borderWidth = 2
     , startupHook = setWMName "LG3D" >> spawn "feh --bg-center /home/flyingleafe/dotfiles/takecare.png"
     , workspaces = myWorkspaces
     , layoutHook = myLayout
     } `additionalKeys` myKeys

myKeys = [ ((0, xF86XK_AudioMute), spawn "amixer set Master toggle")
         , ((0, xF86XK_AudioLowerVolume), spawn "amixer set Master 5%-")
         , ((0, xF86XK_AudioRaiseVolume), spawn "amixer set Master 5%+")
         , ((0, xF86XK_AudioMicMute), spawn "amixer set Capture toggle")
         , ((0, xF86XK_MonBrightnessUp), spawn "light -A 10")
         , ((0, xF86XK_MonBrightnessDown), spawn "light -U 10")
         , ((myModMask .|. shiftMask, xK_l), spawn "dm-tool lock")
         , ((myModMask .|. shiftMask, xK_s), spawn "dm-tool lock & (sleep 2 && systemctl suspend)")
         , ((myModMask .|. shiftMask, xK_a), spawn "sleep 0.2; scrot -s")
         ] ++ [ ((myModMask .|. otherModMask, key), windows $ {-- onCurrentScreen --} action tag)
              | (tag, key)  <- zip (workspaces myConfig) [xK_1 .. xK_9]
              , (otherModMask, action) <- [ (0, W.view) -- was W.greedyView
                                          , (shiftMask, W.shift)]
              ]

myBarToggle :: XConfig Layout -> (KeyMask, KeySym)
myBarToggle cfg = (modMask cfg, xK_b)

myPP :: PP
myPP = sjanssenPP { ppTitle = xmobarColor "#00ee00" "" . shorten 40 }

main = xmonad =<< statusBar "xmobar" myPP myBarToggle myConfig
